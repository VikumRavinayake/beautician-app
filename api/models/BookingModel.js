let mongoose = require('mongoose');
const { Pending } = require('../enums/Status');
const status = require('../enums/Status');

let Schema = mongoose.Schema;

let bookingModelSchema = new Schema({
    customer_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref : 'Customer',
        require : [true, 'Customer_id is required']
    },
    service_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref : 'Service',
        require: [true, 'Service_id is required']
    },
    status:{
        type: String,
        enum : status,
        default:Pending,
        require: [true, 'status is required']
    },
    create_date:{
        type:Date,
        default:Date.now
    }


});

const Booking = mongoose.model(Booking,bookingModelSchema);
module.exports = {Booking}




