let mongoose = require('mongoose');
const { Pending } = require('../enums/Status');
const status = require('../enums/Status');

let Schema = mongoose.Schema;

let paymentModelSchema = new Schema({
    booking_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref : 'Booking',
        require : [true, 'Booking_id is required']
    },
    amout:{
        type: Number,
        require: [true, 'amout is required']
    },
    status:{
        type: String,
        enum : status,
        default: Pending,
        require: [true, 'status is required']
    },
    create_date:{
        type:Date,
        default:Date.now
    }


});

const Payment = mongoose.model(Payment,paymentModelSchema);
module.exports = {Payment}



