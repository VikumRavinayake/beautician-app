let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let beauticianModelSchema = new Schema({
    user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref : 'User',
        require : [true, 'user_id is required']
    },
    business_license_number:{
        type: String, 
        require: false
    },
    create_date:{
        type:Date,
        default:Date.now
    }


});

const Beautician = mongoose.model(Beautician,beauticianModelSchema);
module.exports = {Beautician}




