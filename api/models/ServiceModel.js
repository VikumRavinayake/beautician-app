let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let serviceModelSchema = new Schema({
    title: {
        type : String,
        require:[true,'Title field is required...']
    },
    description: {
        type : String,
        require:[true,'description field is required...']
    },
    image: {
        type : String,
        require:[true,'image field is required...']
    },
    tag_id: {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Tag',
        require:[true,'tag_id field is required...']
    },
    price: {
        type : Number,
        require:[true,'price field is required...']
    },
    beautician_id: {
        type : mongoose.Schema.Types.ObjectId,
        ref:'Beautician',
        require:[true,'image field is required...']
    }
});


const Service = mongoose.model(Service,serviceModelSchema);
module.exports = {Service};




