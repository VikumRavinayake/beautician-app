let mangoose = require('mongoose');
let bcrypt = require('bcrypt');
require('dotenv').config();
let jwt = require('jsonwebtoken');
const userRoles = require('../enums/UserRoles');

let Schema = mongoose.Schema;

let userSchema = new Schema({
    name : {
        type : String,
        require : [true, 'Name field is required'],
        maxlength : 15,
        minlength : 3

    },
    email : {
        type : String,
        require : [true, 'Email field is required'],
        unique : true
        
    },
    username : {
        type : String,
        require : [true, 'Username field is required'],
        unique : true,
        maxlength : 10,
        minlength : 3
        
    },
    password : {
        type : String,
        require : [true, 'Password field is required'],
        minlength : 4
        
    },
    role : {
        type : String,
        enum : userRoles,
        default : CUSTOMER,
        require : [true, 'User role is required']
        
    },
    profile_image :{
        type : String,
        require : false
    },
    phone_number :{
        type : String,
        require : false
    },
    create_date :{
        type : String,
        default : Date.now
    }

});

// Saving user data
UserSchema.pre('save', function (next) {
    var user = this;
    if (user.isModified('password')) {
        //checking if password field is available and modified
        bcrypt.genSalt(SALT, function (err, salt) {
            if (err) return next(err)
        
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) return next(err)
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

const User = mongoose.model(User,userSchema);
module.exports = {User};






   
 
