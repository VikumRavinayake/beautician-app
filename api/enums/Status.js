
const status = Object.freeze({
    Pending : "Pending",
	Accepted : "Accepted",
	Cancelled : "Cancelled",
    Completed : "Completed"

});

module.exports = status; 