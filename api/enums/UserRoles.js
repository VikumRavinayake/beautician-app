/*const userRoles = {
    ADMIN : "ADMIN",
    CUSTOMER : "CUSTOMER",
    BEAUTICIAN : "BEAUTICIAN"
}*/





const userRoles = Object.freeze({
    ADMIN : "ADMIN",
    CUSTOMER : "CUSTOMER",
    BEAUTICIAN : "BEAUTICIAN"
})

module.exports = userRoles;
